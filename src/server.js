// src/server.js
import express from "express";
import * as http from "http";
import { WebSocketServer } from "ws";
import path, { dirname } from "path";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const app = express();
app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname, "/index.html"));
});
const server = http.createServer(app);

function handleOnMessage(ws) {
  return (message) => {
    const messageString = message.toString();
    console.log(`received: ${messageString}`);
    const broadcastRegex = /^broadcast\:/;
    if (broadcastRegex.test(messageString)) {
      const messageBody = messageString.toString().replace(broadcastRegex, "");
      //send back the message to the other clients
      wss.clients.forEach((client) => {
        if (client != ws) {
          client.send(messageBody);
        }
      });
    } else {
      ws.send(messageString);
    }
  };
}
const wss = new WebSocketServer({ server: server, path: "/ws" });
wss.on("connection", (ws) => {
  ws.on("message", handleOnMessage(ws));

  // let count = 0
  // setInterval(() => {
  //   ws.send(`message count: ${++count}`)
  // }, 1000)

  ws.send("Opened connection");
});

const port = 3000;
server.listen(port, () => {
  console.log(`Server started on port ${server.address().port} :)`);
});
